#!/bin/bash
## run while loop to display date and hostname on screen ##
while [ : ]
do
    clear
    tput cup 5 5
    echo "$(date) [ press CTRL+C to stop ]"
    tput cup 6 5
    sleep 28800
done
